﻿Public Class User
    Protected weblists As New List(Of String)
    Protected modes As New List(Of String)

    Public Sub New()
        modes.Add("FacebookFanpage")
        weblists.Add("Likena")
    End Sub

    Public Function getWeblists() As List(Of String)
        Return Me.weblists.Distinct.ToList
    End Function

    Public Function getMode() As List(Of String)
        Return Me.modes.Distinct.ToList
    End Function

    Public Overridable Sub setWebLists(ByVal lists As String())

    End Sub
End Class
