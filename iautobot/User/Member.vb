﻿Public Class Member
    Inherits User
    Public modelists As String() = New String() {"FacebookPhoto", "FacebookAlbum", "FacebookFollow"}

    Public Sub New()
        For Each list As String In modelists : modes.Add(list) : Next
    End Sub

    Public Overrides Sub setWebLists(lists() As String)
        For Each list As String In lists : weblists.Add(list) : Next
    End Sub
End Class
