﻿Imports OpenQA.Selenium
Imports System.Threading
Imports System.ComponentModel

Public Class FacebookAlbum
    Inherits AutoBot

    Public Sub New(ByRef run As Run)
        MyBase.New(run)
    End Sub

    Public Sub New(ByRef run As Run, ByRef Bg_worker As BackgroundWorker, ByRef Bg_event As DoWorkEventArgs)
        MyBase.New(run, Bg_worker, Bg_event)
    End Sub

    Public Sub Start()
        driver.Navigate.GoToUrl(WeblikeObj.getAlbum.ToString)
        MainLoop()
        SetLogs("รอซักครู่กำลัง โหลดหน้าต่อไป....")
        worker.ReportProgress(0, hashmessage)
        Thread.Sleep(5000)
    End Sub

    Public Sub MainLoop()
        For i = 1 To getLoops()
            If stopWorker() Then Exit Sub
            ClickFollowButton()
            If Not SwitchAndMaximizeWindow() Then GoTo NextState
            Thread.Sleep(3000)
            If Not driverHas() Then Exit Sub
            Try
                Dim likebar = driver.FindElement(By.Id("album_feedback_pagelet"))
                If String.IsNullOrEmpty(likebar.GetAttribute("innerHTML")) Then
                    While String.IsNullOrEmpty(likebar.GetAttribute("innerHTML"))
                        driver.FindElement(By.CssSelector("body")).SendKeys(Keys.Control + Keys.End)
                        Thread.Sleep(200)
                    End While
                End If
                CType(driver, IJavaScriptExecutor).ExecuteScript("arguments[0].scrollIntoView();", driver.FindElement(By.XPath("//*[@id='album_feedback_pagelet']")))
                Dim LikeLinks As IList(Of IWebElement) = driver.FindElements(By.TagName("a"))
                Dim likeelement As IWebElement = Nothing
                For Each likelink As IWebElement In LikeLinks
                    If likelink.GetAttribute("outerHTML").IndexOf("UFILikeLink") <> -1 Then
                        likeelement = likelink
                        Exit For
                    End If
                Next
                If likeelement.GetAttribute("outerHTML").IndexOf("UFILinkBright") = -1 Then
                    likeelement.Click()
                    SetLogs("Like สำเร็จ", "success")
                    worker.ReportProgress(0, hashmessage)
                Else
                    SetLogs("Facebook Page Error", "error")
                    worker.ReportProgress(0, hashmessage)
                End If
            Catch ex As Exception
                SetLogs("Facebook Page Error", "error")
                worker.ReportProgress(0, hashmessage)
            End Try
            If Not CloseWindowPopUp() Then Exit Sub
NextState:
            If Not NextState(MyBase.RunForm) Then Exit Sub
        Next
    End Sub
End Class
