﻿Imports OpenQA.Selenium
Imports System.Threading
Imports OpenQA.Selenium.Support.UI
Imports System.ComponentModel

Public Class FacebookPhoto
    Inherits AutoBot

    Public Sub New(ByRef run As Run)
        MyBase.New(run)
    End Sub

    Public Sub New(ByRef run As Run, ByRef Bg_worker As BackgroundWorker, ByRef Bg_event As DoWorkEventArgs)
        MyBase.New(run, Bg_worker, Bg_event)
    End Sub

    Public Sub Start()
        driver.Navigate.GoToUrl(WeblikeObj.getPhoto.ToString)
        MainLoop()
        SetLogs("รอซักครู่กำลัง โหลดหน้าต่อไป....")
        worker.ReportProgress(0, hashmessage)
        Thread.Sleep(5000)
    End Sub

    Private Sub MainLoop()
        For i = 1 To getLoops()
            If stopWorker() Then Exit Sub
            ClickFollowButton()
            If Not SwitchAndMaximizeWindow() Then GoTo NextState
            If Not driverHas() Then Exit Sub
            If FindElement.WaitElement(By.XPath("//div[@id='photos_snowlift']/div[2]/div/a/i")) Is Nothing Then
                SetLogs("Facebook Page Error", "error")
                worker.ReportProgress(0, hashmessage)
                If Not CloseWindowPopUp() Then Exit Sub
                GoTo NextState
            End If
            Try
                Dim LikeAction As IWebElement = driver.FindElement(By.Id("fbPhotoSnowliftButtons"))
                Dim linkaction As IList(Of IWebElement) = LikeAction.FindElements(By.TagName("a"))
                Dim likeelement As IWebElement = Nothing
                For Each link As IWebElement In linkaction
                    If link.GetAttribute("outerHTML").IndexOf("UFILikeLink") <> -1 Then likeelement = link
                Next
                If likeelement.GetAttribute("outerHTML").IndexOf("UFILinkBright") = -1 Then
                    likeelement.Click()
                    SetLogs("Like สำเร็จ", "success")
                    worker.ReportProgress(0, hashmessage)
                Else
                    SetLogs("Facebook Page Error", "error")
                    worker.ReportProgress(0, hashmessage)
                End If
            Catch ex As Exception
                SetLogs("Facebook Page Error", "error")
                worker.ReportProgress(0, hashmessage)
            End Try
            If Not CloseWindowPopUp() Then Exit Sub
NextState:
            If Not NextState(MyBase.RunForm) Then Exit Sub
        Next
    End Sub
End Class
