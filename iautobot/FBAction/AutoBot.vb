﻿Imports System.Text
Imports OpenQA.Selenium
Imports OpenQA.Selenium.Firefox
Imports OpenQA.Selenium.Chrome
Imports OpenQA.Selenium.Support.UI
Imports OpenQA.Selenium.PhantomJS
Imports System.Threading
Imports System.Text.RegularExpressions
Imports iAutobot
Imports System.ComponentModel

Public Class AutoBot
    Implements IDisposable
    Public Id_Follow As String = String.Empty
    Public RunForm As Run = Nothing
    Public worker As BackgroundWorker = Nothing
    Public e As DoWorkEventArgs = Nothing
    Public hashmessage As New Hashtable From {{"message", ""}, {"color", ""}}

    Public Sub New(ByRef run As Run, ByRef Bg_worker As BackgroundWorker, ByRef Bg_event As DoWorkEventArgs)
        RunForm = run
        worker = Bg_worker
        e = Bg_event
    End Sub

    Public Sub New(run As Run)
        RunForm = run
    End Sub

    Public Sub New()

    End Sub

    Public Sub Main()
        WebDrivers.CreatePhantomJS()
        If worker.CancellationPending Then e.Cancel = True : Exit Sub
        SetLogs("เริ่มต้นการใช้งานสำเร็จ")
        worker.ReportProgress(0, hashmessage)
    End Sub

    Public Sub Init()
        If stopWorker() Then Exit Sub
        SignInWebPage()

        If stopWorker() Then Exit Sub
        SetTextCoins()

        If stopWorker() Then Exit Sub
        NewTab()

        If stopWorker() Then Exit Sub
        SignInFacebook()

        If stopWorker() Then Exit Sub
        CloseTab()
    End Sub

    Public Sub SignInWebPage()
        driver.Navigate().GoToUrl(WeblikeObj.getUrl.ToString)
        driver.FindElement(By.Name("login")).SendKeys(WebUsername)
        driver.FindElement(By.Name("pass")).SendKeys(WebPassword)
        driver.FindElement(By.Name("connect")).Click()
        Thread.Sleep(2000)
        If IsNothing(FindElement.ElementHas(By.Name("connect"))) Then
            SetLogs(logsMsg("loginsuccess"), "success")
            worker.ReportProgress(0, hashmessage)
        Else
            SetLogs(logsMsg("loginfail"), "error")
            worker.ReportProgress(0, hashmessage)
            RunForm.CancelMsg = "หยุดการใช้งานไม่สามารถเข้่าสู่ระบบได้"
            worker.CancelAsync()
        End If
    End Sub

    Public Sub SignInFacebook()
        driver.Navigate.GoToUrl("http://www.facebook.com")
        Thread.Sleep(2000)
        driver.FindElement(By.Id("email")).SendKeys(FacebookUsername)
        driver.FindElement(By.Id("pass")).SendKeys(FacebookPassword)
        driver.FindElement(By.Id("loginbutton")).Click()
        Thread.Sleep(2000)
        If IsNothing(FindElement.ElementHas(By.Id("loginbutton"))) Then
            SetLogs(logsMsg("facebookloginsuccess"), "success")
            worker.ReportProgress(0, hashmessage)
        Else
            SetLogs(logsMsg("facebookloginfail"), "error")
            worker.ReportProgress(0, hashmessage)
            RunForm.CancelMsg = "หยุดการใช้งานไม่สามารถเข้่า facebook ได้"
            worker.CancelAsync()
        End If
    End Sub

    Public Sub NewTab()
        driver.FindElement(By.CssSelector("body")).SendKeys(Keys.Control + "t")
        driver.SwitchTo.Window(driver.WindowHandles.Last)
    End Sub

    Public Sub CloseTab()
        driver.FindElement(By.CssSelector("body")).SendKeys(Keys.Control + "w")
        driver.SwitchTo.Window(driver.WindowHandles.First)
    End Sub

    Public Sub ClickFollowButton()
        If stopWorker() Then Exit Sub
        If Not driverHas() Then Exit Sub
        SetTextCoins()
        WaitMsg(False)
        Id_Follow = String.Empty
        If findFollow("follow_vip") Then Exit Sub
        If findFollow("follow") Then Exit Sub
    End Sub

    Public Sub WaitMsg(ByVal bool As Boolean)
        If stopWorker() Then Exit Sub
        If bool Then Exit Sub
        Dim hint = FindElement.WaitElement(By.XPath("//div[@id='Hint']"))
        If IsNothing(hint) Then Exit Sub
        If String.IsNullOrEmpty(hint.GetAttribute("innerHTML")) Then Exit Sub
        Dim msgbox = FindElement.WaitElement(By.XPath("//div[@id='Hint']/div"))
        If Not IsNothing(msgbox) Then Exit Sub
        ClickSkip()
        WaitMsg(bool)
    End Sub

    Public Sub ClickSkip()
        If worker.CancellationPending Then e.Cancel = True : Exit Sub
        If String.IsNullOrEmpty(Id_Follow) Then Exit Sub
        Dim skiplink = FindElement.ElementHas(By.Id(Id_Follow))
        If IsNothing(skiplink) Then Exit Sub
        Dim clickable
        Try
            clickable = FindElement.WaitToClick(skiplink.FindElement(By.LinkText(WeblikeObj.getSkip.ToString)))
        Catch ex As Exception
            clickable = Nothing
        End Try
        If IsNothing(clickable) Then Exit Sub
        clickable.Click()
        SetLogs("Skip", "primary")
        worker.ReportProgress(0, hashmessage)
    End Sub

    Public Function SwitchAndMaximizeWindow() As Boolean
        If stopWorker() Then Exit Function
        If Not driverHas() Then Exit Function
        Thread.Sleep(2000)
        Try
            If driver.WindowHandles.Count > 1 Then
                driver.SwitchTo.Window(driver.WindowHandles.Last)
                driver.Manage.Window.Maximize()
                SetLogs(driver.Title)
                worker.ReportProgress(0, hashmessage)
                Return True
            End If
        Catch ex As Exception
            Return False
        End Try
        Return False
    End Function

    Public Function getLoops() As Integer
        If stopWorker() Then Exit Function
        Dim round As Integer = 0
        Dim follow = FindElement.ElementHave(By.ClassName("follow"))
        Dim follow_vip = FindElement.ElementHave(By.ClassName("follow_vip"))
        If Not IsNothing(follow) Then round += follow.Count
        If Not IsNothing(follow_vip) Then round += follow_vip.Count
        Return round
    End Function

    Public Function CloseWindowPopUp() As Boolean
        If stopWorker() Then Exit Function
        If Not driverHas() Then Exit Function
        Thread.Sleep(2000)
        Try
            If driver.WindowHandles.Count > 1 Then
                driver.SwitchTo.Window(driver.WindowHandles.Last).Close()
                driver.SwitchTo().Window(driver.WindowHandles.First)
            Else
                driver.SwitchTo().Window(driver.WindowHandles.First)
            End If
        Catch ex As Exception
            RunForm.windowcrash = True
            Return False
        End Try
        Thread.Sleep(2000)
        Dim msgtext = FindElement.WaitElement(By.XPath("//div[@id='Hint']/div"), 60)
        If IsNothing(msgtext) Then ClickSkip() : Exit Function
        Dim logs
        Dim status
        Try
            logs = msgtext.FindElement(By.TagName("div")).GetAttribute("innerHTML")
            status = msgtext.FindElement(By.TagName("div")).GetAttribute("class")
        Catch ex As Exception
            logs = "ไม่สามารถนำค่ามาแสดงได้"
            status = "error"
        End Try
        SetLogs(StripTags(logs), status)
        worker.ReportProgress(0, hashmessage)
        If status.Equals("error") Then ClickSkip()
        Return True
    End Function

    Protected Sub SetTextCoins()
        If stopWorker() Then Exit Sub
        Try
            Dim coins = driver.FindElement(By.Id("c_coins"))
            RunForm.SetCoins(coins.GetAttribute("innerHTML").ToString)
        Catch ex As Exception
        End Try
    End Sub

    Public Function stopWorker() As Boolean
        If worker.CancellationPending Then e.Cancel = True : Return True
        Return False
    End Function

    Protected Overloads Sub SetLogs(ByVal text As String, ByVal key As String)
        hashmessage.Item("message") = text
        hashmessage.Item("color") = LogsColor.Item(key)
    End Sub

    Protected Function NextState(childRun As Run) As Boolean
        Try
            driver.SwitchTo.Window(driver.WindowHandles.FirstOrDefault)
            Return True
        Catch ex As Exception
            childRun.windowcrash = True
            Return False
        End Try
    End Function

    Protected Overloads Sub SetLogs(ByVal text As String)
        hashmessage.Item("message") = text
        hashmessage.Item("color") = LogsColor.Item("default")
    End Sub

    Public Shared Sub DebugMode()
        driver = New FirefoxDriver
        'driver = New PhantomJSDriver
        driver.Navigate.GoToUrl("http://www.facebook.com")
        driver.FindElement(By.Name("email")).SendKeys("pathana13@gmail.com")
        driver.FindElement(By.Name("pass")).SendKeys("pathana24")
        driver.FindElement(By.Id("loginbutton")).Click()
        driver.Navigate.GoToUrl("https://www.facebook.com/an.aiinook/photos/?tab=album&album_id=1591409687784332")
        Dim likebar = driver.FindElement(By.Id("album_feedback_pagelet"))
        If String.IsNullOrEmpty(likebar.GetAttribute("innerHTML")) Then
            While String.IsNullOrEmpty(likebar.GetAttribute("innerHTML"))
                'CType(driver, IJavaScriptExecutor).ExecuteScript()
                driver.FindElement(By.CssSelector("body")).SendKeys(Keys.Control + Keys.End)
                Thread.Sleep(200)
            End While
        End If
        Console.WriteLine("find A")
        CType(driver, IJavaScriptExecutor).ExecuteScript("arguments[0].scrollIntoView();", driver.FindElement(By.XPath("//*[@id='album_feedback_pagelet']")))
        Try
            Dim LikeLinks As IList(Of IWebElement) = driver.FindElements(By.TagName("a"))
            Dim likeelement As IWebElement = Nothing
            For Each likelink As IWebElement In LikeLinks
                If likelink.GetAttribute("outerHTML").IndexOf("UFILikeLink") <> -1 Then
                    likeelement = likelink
                    Exit For
                End If
            Next
            If likeelement.GetAttribute("outerHTML").IndexOf("UFILinkBright") = -1 Then
                likeelement.Click()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Function findFollow(ByVal name As String) As Boolean
        If stopWorker() Then Exit Function
        Dim follows = FindElement.ElementHave(By.ClassName(name))
        If follows Is Nothing Then Return False
        If follows.Count = 0 Then Return False
        For Each follow As IWebElement In follows
            If String.IsNullOrEmpty(follow.GetAttribute("style")) Then
                Try
                    follow.FindElement(By.ClassName("followbutton")).Click()
                    Id_Follow = follow.GetAttribute("id")
                    Return True
                Catch ex As Exception
                    Return False
                End Try
            End If
        Next
        Return False
    End Function

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        ' TODO: uncomment the following line if Finalize() is overridden above.
        ' GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
