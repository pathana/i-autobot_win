﻿Imports OpenQA.Selenium
Imports System.Threading
Imports System.ComponentModel

Public Class FacebookFollow
    Inherits AutoBot

    Public Sub New(ByRef run As Run)
        MyBase.New(run)
    End Sub

    Public Sub New(ByRef run As Run, ByRef Bg_worker As BackgroundWorker, ByRef Bg_event As DoWorkEventArgs)
        MyBase.New(run, Bg_worker, Bg_event)
    End Sub

    Public Sub Start()
        driver.Navigate.GoToUrl(WeblikeObj.getFollow.ToString)
        MainLoop()
        SetLogs("รอซักครู่กำลัง โหลดหน้าต่อไป....")
        worker.ReportProgress(0, hashmessage)
        Thread.Sleep(5000)
    End Sub

    Private Sub MainLoop()
        For i = 1 To getLoops()
            If stopWorker() Then Exit Sub
            ClickFollowButton()
            If Not SwitchAndMaximizeWindow() Then GoTo NextState
            Thread.Sleep(5000)
            If Not driverHas() Then Exit Sub
            If findFollowing() = False Then
                findFollowButton()
            End If
            If Not CloseWindowPopUp() Then Exit Sub
NextState:
            If Not NextState(MyBase.RunForm) Then Exit Sub
        Next
    End Sub

    Private Function findFollowing() As Boolean
        Try
            Dim buttons As IList(Of IWebElement) = driver.FindElements(By.TagName("button"))
            For Each button As IWebElement In buttons
                If button.GetAttribute("innerHTML").IndexOf("กำลังติดตาม") <> -1 Xor button.GetAttribute("innerHTML").IndexOf("Following") <> -1 Then
                    SetLogs("Facebook Page Error", "error")
                    worker.ReportProgress(0, hashmessage)
                    Return True
                End If
            Next
        Catch ex As Exception
            Return False
        End Try
        Return False
    End Function

    Private Sub findFollowButton()
        Try
            Dim links As IList(Of IWebElement) = driver.FindElements(By.TagName("a"))
            Dim linkStr As String
            For Each link As IWebElement In links
                linkStr = link.GetAttribute("innerHTML")
                If linkStr.IndexOf("ติดตาม") <> -1 Xor linkStr.IndexOf("Follow") <> -1 Then
                    link.Click()
                    SetLogs("Like สำเร็จ", "success")
                    worker.ReportProgress(0, hashmessage)
                    Exit For
                End If
            Next
        Catch ex As Exception
            SetLogs("Facebook Page Error", "error")
            worker.ReportProgress(0, hashmessage)
        End Try
    End Sub
End Class
