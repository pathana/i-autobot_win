﻿Imports OpenQA.Selenium
Imports OpenQA.Selenium.Support
Imports OpenQA.Selenium.Firefox
Imports System.Threading
Imports System.ComponentModel

Public Class FacebookFanPage
    Inherits AutoBot

    Public Sub New(ByRef run As Run)
        MyBase.New(run)
    End Sub

    Public Sub New(ByRef run As Run, ByRef Bg_worker As BackgroundWorker, ByRef Bg_event As DoWorkEventArgs)
        MyBase.New(run, Bg_worker, Bg_event)
    End Sub

    Public Sub Start()
        driver.Navigate.GoToUrl(WeblikeObj.getFanpage.ToString)
        MainLoop()
        SetLogs("รอซักครู่กำลัง โหลดหน้าต่อไป....")
        worker.ReportProgress(0, hashmessage)
        Thread.Sleep(5000)
    End Sub

    Private Sub MainLoop()
        For i = 1 To getLoops()
            If stopWorker() Then Exit Sub
            ClickFollowButton()
            If Not SwitchAndMaximizeWindow() Then GoTo NextState
            Thread.Sleep(5000)
            If Not driverHas() Then Exit Sub
            If findUnLike() = False Then
                findLikeButton()
            Else
                SetLogs("Facebook Page Error", "error")
                worker.ReportProgress(0, hashmessage)
            End If
            If Not CloseWindowPopUp() Then Exit Sub
NextState:
            If Not NextState(MyBase.RunForm) Then Exit Sub
        Next
    End Sub

    Private Function findUnLike() As Boolean
        If HasSpanId() Then Return HasButtonLiked() Else Return HasSpanLiked()
        SetLogs("Facebook Page Error", "error")
        worker.ReportProgress(0, hashmessage)
    End Function

    Private Function HasSpanId() As Boolean
        Try
            Dim span As IWebElement = driver.FindElement(By.Id("pagesHeaderLikeButton"))
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Private Function HasSpanLiked() As Boolean
        Try
            Dim tags As IList(Of IWebElement) = driver.FindElements(By.TagName("a"))
            For Each tag As IWebElement In tags
                If tag.GetAttribute("outerHTML").IndexOf("likedButton") <> -1 Then Return True
            Next
        Catch ex As Exception
            Return False
        End Try
        Return False
    End Function

    Private Function HasButtonLiked() As Boolean
        Try
            Dim button As IWebElement = driver.FindElement(By.Id("pagesHeaderLikeButton"))
            Dim strLiked As String = button.GetAttribute("innerHTML")
            If strLiked.IndexOf("Liked") <> -1 Xor strLiked.IndexOf("ถูกใจแล้ว") <> -1 Then Return True
        Catch ex As Exception
            Return False
        End Try
        Return False
    End Function

    Private Sub findLikeButton()
        Try
            Dim tags As IList(Of IWebElement) = driver.FindElements(By.TagName("button"))
            For Each tag As IWebElement In tags
                Dim likeStr As String = tag.GetAttribute("innerHTML")
                If likeStr.IndexOf("ถูกใจ") <> -1 Xor likeStr.IndexOf("Like") <> -1 Then
                    tag.Click()
                    SetLogs("Like สำเร็จ", "success")
                    worker.ReportProgress(0, hashmessage)
                    Exit For
                End If
            Next
        Catch ex As Exception
            SetLogs("Facebook Page Error", "error")
            worker.ReportProgress(0, hashmessage)
        End Try
    End Sub
End Class
