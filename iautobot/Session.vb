﻿Imports System.Text
Imports System.Text.RegularExpressions
Imports Newtonsoft.Json.Linq
Imports OpenQA.Selenium
Imports OpenQA.Selenium.Firefox
Imports OpenQA.Selenium.Support
Module Session
    Public FacebookUsername As String
    Public FacebookPassword As String
    Public WebUsername As String
    Public WebPassword As String
    Public indexWeb As Integer
    Public WeblikeName As String
    Public WeblikeObj As LikeModel
    Public driver As IWebDriver
    Public FBFanpageLoops As Integer = 0
    Public FBAlbumLoops As Integer = 0
    Public FBPhotoLoops As Integer = 0
    Public FBFollowLoops As Integer = 0
    Public FBVideoLoops As Integer = 0
    Public UserDataJson As JObject = Nothing
    Public LogsColor As New Dictionary(Of String, Color) From {{"default", Color.White}, {"error", Color.Red}, {"success", Color.Lime}, {"primary", Color.DeepSkyBlue}, {"info", Color.LightSkyBlue}}
    Public version_number As String = Reflection.Assembly.GetExecutingAssembly.GetName.Version.ToString
    Public version As String = "  Ver " & version_number
    Public Function driverHas() As Boolean
        If driver IsNot Nothing Then Return True Else Return False
    End Function

    Public Function StripTags(ByVal html As String) As String
        Return Regex.Replace(html, "<.*?>", "")
    End Function

    Public Function logsMsg(key As String) As String
        Dim hash As New Hashtable
        hash.Add("loginsuccess", "เข้าสู่ระบบสำเร็จ")
        hash.Add("facebookloginsuccess", "Facebook เข้าสู่ระบบสำเร็จ")
        hash.Add("loginfail", "เข้าสู่ระบบไม่สำเร็จ")
        hash.Add("facebookloginfail", "Facebook เข้าสู่ระบบไม่สำเร็จ")
        Return hash.Item(key)
    End Function
End Module
