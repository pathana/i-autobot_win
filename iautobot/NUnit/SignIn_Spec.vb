﻿Imports NUnit.Framework
Imports OpenQA.Selenium
Imports OpenQA.Selenium.Support
Imports OpenQA.Selenium.Firefox
Imports OpenQA.Selenium.PhantomJS
Imports OpenQA.Selenium.Chrome
Imports System.Threading
Imports OpenQA.Selenium.Remote
Imports System.IO

Namespace UnitTest
    <TestFixture>
    <Parallelizable>
    Public Class SignIn
        Dim autobotTest As AutoBot
        Dim r As Run
        <SetUp>
        Public Sub Initialize()
            r = New Run(New List(Of String))
            autobotTest = New AutoBot(r)
            'Dim services As FirefoxDriverService = FirefoxDriverService.CreateDefaultService
            'services.FirefoxBinaryPath = "C:\Program Files\Mozilla Firefox\firefox.exe"
            'driver = New FirefoxDriver(services)
            Dim options As PhantomJSOptions = New PhantomJSOptions
            Dim userAgent As String = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0"
            options.AddAdditionalCapability("phantomjs.page.settings.userAgent", userAgent)
            driver = New PhantomJSDriver(options)
        End Sub

        <SetUp>
        Public Sub setupSession()
            WeblikeObj = New Likena
            WebUsername = "pathana"
            WebPassword = "nongna13"
            FacebookUsername = "pathana13@gmail.com"
            FacebookPassword = "pathana24"
        End Sub

        <TearDown>
        Public Sub EndTest()
            driver.Quit()
        End Sub

        <Test>
        Public Sub GetHeaders()
            driver.Navigate.GoToUrl("http://192.168.99.100/get_browser.php")
            Using outputFile As New StreamWriter("C:\Users\pathana\iautobot_php\phantomjs.html", True)
                outputFile.WriteLine(driver.PageSource)
            End Using
        End Sub

        <Test>
        Public Sub SyslikeSignIn()
            driver.Navigate.GoToUrl("http://syslike.org")
            driver.FindElement(By.Name("login")).SendKeys(WebUsername)
            driver.FindElement(By.Name("pass")).SendKeys(WebPassword)
            driver.FindElement(By.Name("connect")).Click()
            Thread.Sleep(5000)
            driver.FindElement(By.Id("c_coins"))
            Using outputFile As New StreamWriter("C:\Users\pathana\iautobot_php\syslike_coins.txt", True)
                outputFile.WriteLine(driver.FindElement(By.Id("c_coins")).GetAttribute("innerHTML"))
            End Using
        End Sub

        Public Shared WebModel As LikeModel() = New LikeModel() {New Likena, New Syslike}

        <Test, TestCaseSource("WebModel")>
        Public Sub SignInWebpage_Fail(webmodel As LikeModel)
            WeblikeObj = webmodel
            WebUsername = "test"
            autobotTest.SignInWebPage()
            StringAssert.AreEqualIgnoringCase(r.RichTextBox1.Lines(0).ToString, logsMsg("loginfail"))
            Assert.IsFalse(r.LoopAction.IsBusy)
        End Sub

        <Test, TestCaseSource("WebModel")>
        Public Sub SiginInWebPage_Success(webmodel As LikeModel)
            WeblikeObj = webmodel
            autobotTest.SignInWebPage()
            StringAssert.AreEqualIgnoringCase(r.RichTextBox1.Lines(0).ToString, logsMsg("loginsuccess"))
        End Sub

        <Test>
        Public Sub SignInFacebook_Success()
            autobotTest.SignInFacebook()
            StringAssert.AreEqualIgnoringCase(r.RichTextBox1.Lines(0).ToString, logsMsg("facebookloginsuccess"))
        End Sub

        <Test>
        Public Sub Share_Page()
            driver.Navigate.GoToUrl("http://www.facebook.com")
            Thread.Sleep(2000)
            driver.FindElement(By.Id("email")).SendKeys(FacebookUsername)
            driver.FindElement(By.Id("pass")).SendKeys(FacebookPassword)
            driver.FindElement(By.Id("loginbutton")).Click()
            Thread.Sleep(5000)
            driver.Navigate.GoToUrl("https://m.facebook.com/sharer.php?sid=517659681751610&referrer=pages_feed")
            Dim txtarea = driver.FindElement(By.Id("share_msg_input"))
            txtarea.SendKeys("Test Share Free User")
            driver.FindElement(By.Id("share_submit")).Click()
        End Sub

        <Test>
        Public Sub SignInFacebook_Fail()
            FacebookUsername = "test"
            autobotTest.SignInFacebook()
            StringAssert.AreEqualIgnoringCase(r.RichTextBox1.Lines(0).ToString, logsMsg("facebookloginfail"))
            Assert.IsFalse(r.LoopAction.IsBusy)
        End Sub
    End Class
End Namespace
