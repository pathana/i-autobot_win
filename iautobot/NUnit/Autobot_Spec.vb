﻿Imports NUnit.Framework
Imports OpenQA.Selenium
Imports OpenQA.Selenium.Support
Imports OpenQA.Selenium.Firefox
Imports OpenQA.Selenium.PhantomJS
Imports OpenQA.Selenium.Chrome
Imports JQSelenium
Namespace UnitTest
    <TestFixture>
    <Parallelizable>
    Public Class Autobot_Spec
        Dim autobotTest As AutoBot
        Dim r As Run

        <SetUp>
        Public Sub initAuthen()
            WeblikeObj = New Likena
            WebUsername = "pathana"
            WebPassword = "nongna13"
            FacebookUsername = "pathana13@gmail.com"
            FacebookPassword = "pathana24"
            r = New Run(New List(Of String))
            autobotTest = New AutoBot(r)
            driver = New PhantomJSDriver
            'Dim services As FirefoxDriverService = FirefoxDriverService.CreateDefaultService
            'services.FirefoxBinaryPath = "C:\Program Files\Mozilla Firefox\firefox.exe"
            'driver = New FirefoxDriver(services)
            'autobotTest.Init()
        End Sub

        <TearDown>
        Public Sub EndTest()
            'driver.Quit()
        End Sub

        <Test>
        Public Sub ClickFollow_First()
            driver.Navigate.GoToUrl(WeblikeObj.getFanpage.ToString)
            autobotTest.ClickFollowButton()
            Assert.AreEqual(driver.WindowHandles.Count, CInt(2))
        End Sub

        <Test>
        Public Sub ClickFollow_HasMessage()
            autobotTest.SignInWebPage()
            Threading.Thread.Sleep(200)
            driver.Navigate.GoToUrl(WeblikeObj.getFanpage.ToString)
            Dim hint = driver.FindElement(By.Id("Hint"))
            Dim msg = "var div = document.createElement('DIV');"
            msg += "div.className = 'msg';"
            msg += "var div2 = document.createElement('DIV');"
            msg += "div2.className = 'success';"
            msg += "div2.innerHTML = 'test';"
            msg += "div.appendChild(div2);"
            msg += "arguments[0].appendChild(div);"
            CType(driver, IJavaScriptExecutor).ExecuteScript(msg, hint)
            Threading.Thread.Sleep(1000)
            autobotTest.ClickFollowButton()
            Assert.AreEqual(driver.WindowHandles.Count, CInt(2))
        End Sub

        <Test>
        Public Sub FindFanPage()
            driver.Navigate().GoToUrl(WeblikeObj.getUrl.ToString)
            driver.FindElement(By.Name("login")).SendKeys(WebUsername)
            driver.FindElement(By.Name("pass")).SendKeys(WebPassword)
            driver.FindElement(By.Name("connect")).Click()
            Threading.Thread.Sleep(2000)
            driver.Navigate.GoToUrl(WeblikeObj.getFanpage.ToString)
            Dim cmd As String
            cmd = "var a = arguments[0].getElementsByTagName('A')[0];"
            cmd += "var onclick = a.getAttribute('onclick');"
            cmd += "var start = onclick.indexOf('?') + 1;"
            cmd += "var fb = onclick.substring(start);"
            cmd += "var page = fb.substring(0, fb.indexOf(',') - 1);"
            cmd += "return page;"

            Dim fbpage As New List(Of String)
            Dim id_class As New List(Of String)
            Dim round As Integer = 0
            Dim follow = FindElement.ElementHave(By.ClassName("follow"))
            Dim follow_vip = FindElement.ElementHave(By.ClassName("follw_vip"))
            Dim result
            If Not IsNothing(follow) Then round += follow.Count Else round = 0
            For i = 1 To round
                If Not IsNothing(follow) Then
                    For Each f As IWebElement In follow
                        id_class.Add(f.GetAttribute("id"))
                        result = CType(driver, IJavaScriptExecutor).ExecuteScript(cmd, f)
                        fbpage.Add(CStr(result))
                    Next
                End If
            Next
            round = 0
            If Not IsNothing(follow_vip) Then round += follow_vip.Count Else round = 0
            For i = 1 To round
                If Not IsNothing(follow_vip) Then
                    For Each f As IWebElement In follow_vip
                        id_class.Add(f.GetAttribute("id"))
                        fbpage.Add(CType(driver, IJavaScriptExecutor).ExecuteScript(cmd, f))
                    Next
                End If
            Next


            Dim file As System.IO.StreamWriter = New System.IO.StreamWriter("C:\Users\pathana\pagefb.txt", True)
            For Each i As String In fbpage
                file.WriteLine(i)
            Next

        End Sub

        <Test>
        Public Sub ClickFollow_HasImg()
            autobotTest.SignInWebPage()
            Threading.Thread.Sleep(200)
            driver.Navigate.GoToUrl(WeblikeObj.getFanpage.ToString)
            autobotTest.ClickFollowButton()
            driver.SwitchTo.Window(driver.WindowHandles.Last)
            Threading.Thread.Sleep(1000)
            driver.Close()
            driver.SwitchTo.Window(driver.WindowHandles.First)
            Threading.Thread.Sleep(5000)
            Dim del = "var hint = document.getElementById('Hint');"
            del += "while (hint.hasChildNodes()) {"
            del += "hint.removeChild(hint.firstChild);}"
            CType(driver, IJavaScriptExecutor).ExecuteScript(del)
            Threading.Thread.Sleep(5000)
            Dim hint = driver.FindElement(By.Id("Hint"))
            Dim msg = "var div = document.createElement('IMG');"
            msg += "var s = document.createAttribute('src');"
            msg += "s.value = 'img/loader.gif';"
            msg += "div.setAttributeNode(s);"
            msg += "arguments[0].appendChild(div);"
            CType(driver, IJavaScriptExecutor).ExecuteScript(msg, hint)
            Threading.Thread.Sleep(1000)
            autobotTest.ClickFollowButton()
        End Sub

    End Class
End Namespace
