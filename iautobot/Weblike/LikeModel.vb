﻿Public Class LikeModel
    Protected link As String = String.Empty
    Protected modes As New List(Of String)
    Private modelists As String() = New String() {"FacebookFanPage", "FacebookPhoto", "FacebookAlbum", "FacebookFollow"}

    Public Sub New()
        For Each modelist As String In modelists : modes.Add(modelist) : Next
    End Sub

    Public Function getUrl() As String
        Return link
    End Function

    Public Overridable Function getSkip() As String
        Return "skip"
    End Function

    Public Function getFanpage() As String
        Return link & "p.php?p=facebook"
    End Function

    Public Function getPhoto() As String
        Return link & "p.php?p=fb_photo"
    End Function

    Public Overridable Function getAlbum() As String
        Return link & "p.php?p=fb_album"
    End Function

    Public Function getFollow() As String
        Return link & "p.php?p=fb_subscribe"
    End Function

    Public Function getMode() As List(Of String)
        Return Me.modes.Distinct.ToList
    End Function
End Class
