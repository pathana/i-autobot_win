﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports Newtonsoft.Json.Linq
Imports System.Runtime.InteropServices

Public Class LoginForm1
    Dim dictData As New Dictionary(Of String, Object)
    Dim UserData As JObject = Nothing

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        setKey()
        dictData.Clear()
        dictData.Add("key", KeyTextBox.Text)
        If Not Validate_key() Then Exit Sub
        If Not updateStatus() Then MsgBox("ปรับสถานะไม่สำเร็จ") : Exit Sub
        UserDataJson = UserData
        AutobotForm.Show()
        Me.Hide()
    End Sub

    Public Function Validate_key() As Boolean
        Dim jsonData = Api.postData(dictData, My.Settings.URL & "api/validate_key.php")
        If Not Validate_Version(jsonData) Then Return False
        If jsonData.ContainsKey("data") Then UserData = jsonData.Item("data")
        If UserData IsNot Nothing Then
            Return True
        Else
            MsgBox("Key นี้หมดอายุหรือกำลังถูกใช้งานอยู่")
            Return False
        End If
    End Function

    Private Function updateStatus() As Boolean
        Dim jsonData = Api.postData(dictData, My.Settings.URL & "api/updateStatus.php")
        If jsonData.ContainsKey("data") Then Return jsonData.Item("data")
        Return False
    End Function

    Private Function Validate_Version(data As Dictionary(Of String, Object)) As Boolean
        If data.ContainsKey("v") Then
            Dim version As JObject = data.Item("v")
            If String.Compare(version.Item("version"), version_number) < 0 Then
                MsgBox("Version ไม่ตรง กรุณาอัพเดท version")
                Return False
            End If
            Return True
        End If
        Return False
    End Function

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        KeyTextBox.Clear()
    End Sub

    Private Sub LoginForm1_Load(sender As Object, e As EventArgs) Handles Me.Load
        If My.Settings.AppCheck Then KeyTextBox.Text = My.Settings.Key : LoginCheck.Checked = True
        Dim CurDir As String = My.Application.Info.DirectoryPath
        If CurDir.IndexOf("Debug") <> -1 Then My.Settings.URL = "http://192.168.99.100/" Else My.Settings.URL = "http://i-autobot.com/"
        Text = Text & version
    End Sub

    Private Sub setKey()
        My.Settings.Key = KeyTextBox.Text
        My.Settings.AppCheck = LoginCheck.Checked
    End Sub

End Class
