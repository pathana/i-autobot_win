﻿Imports System.ComponentModel
Imports System.Security.Cryptography
Imports System.Threading
Imports Newtonsoft.Json.Linq

Public Class FacebookForm
    Dim Fbmode As New List(Of String)
    Dim FBChkList As New List(Of String)
    Dim FBRoundList As New List(Of String)
    Dim thd As Thread
    Private Sub FacebookForm_Load(sender As Object, e As EventArgs) Handles Me.Load
        'webComboInit()
        Web.Text = "Weblike : " & UserDataJson.Item("Web_like_name").ToString
        WeblikeObj = Activator.CreateInstance(Type.GetType("iAutobot." & UserDataJson.Item("Web_like_name").ToString))
        userInfoInit()
        addFBMode()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        AssignValue()
        AutobotForm.Hide()
        BackgroundWorker1.RunWorkerAsync()
    End Sub

    Private Sub FBStart()
        'Try
        '    Dim autobot As New AutoBot
        '    autobot.setRunForm(New Run)
        '    autobot.Main()
        '    autobot.Init()
        '    For Each mode As String In Fbmode
        '        If Not KeyAvailable() Then
        '            driver.Quit()
        '            MsgBox("มีการใช้ key ซ้ำในระบบ")
        '            Exit For
        '        End If
        '        Dim MyInstance As Object = Activator.CreateInstance(Type.GetType("LikenaVB." & mode))
        '        MyInstance.Start()
        '    Next
        '    driver.Quit()
        '    Button1.Show()
        '    MsgBox("Success")
        'Catch ex As Exception
        '    Console.WriteLine("Errror " & ex.Message)
        '    MsgBox("ขออภัยที่ต้องปิด webbrowser")
        '    Button1.Show()
        'End Try
    End Sub


    Private Sub AssignFBMode()
        Fbmode.Clear()
        'If FacebookFanpageCheckbox.Checked Then For i = 1 To FBFanpageLoops : Fbmode.Add("FacebookFanPage") : Next
        'If FacebookAlbumCheckbox.Checked Then For i = 1 To FBAlbumLoops : Fbmode.Add("FacebookAlbum") : Next
        'If FacebookPhotoCheckbox.Checked Then For i = 1 To FBPhotoLoops : Fbmode.Add("FacebookPhoto") : Next
        'If FacebookFollowCheckbox.Checked Then For i = 1 To FBFollowLoops : Fbmode.Add("FacebookFollow") : Next
        'If FacebookVideoCheckbox.Checked Then For i = 1 To FBVideoLoops : Fbmode.Add("FacebookVideo") : Next
        ShuffleIList(Fbmode)
    End Sub

    Private Sub AssignValue()
        AssignUserInfo()
        AssignFBInfo()
        AssignMySettings()
        'AssignFBMode()
    End Sub

    Private Sub AssignUserInfo()
        FacebookUsername = FacebookUserText.Text
        FacebookPassword = FacebookPassText.Text
        WebUsername = WeblikeUserText.Text
        WebPassword = WeblikePassText.Text
    End Sub

    Private Sub AssignFBInfo()
        Fbmode.Clear()
        For Each mode As String In WeblikeObj.getMode()
            If CType(Me.Controls.Item(mode & "Checkbox"), CheckBox).Checked Then
                For i = 1 To CType(Me.Controls.Item(mode & "Round"), NumericUpDown).Value : Fbmode.Add(mode) : Next
            End If
        Next
        ShuffleIList(Fbmode)
        'WeblikeName = DirectCast(WebComboBox.SelectedItem, KeyValuePair(Of String, String)).Key
    End Sub

    Public Shared Function ShuffleIList(Of T)(inputList As IList(Of T)) As IList(Of T)
        Dim cryptoServiceProvider = New RNGCryptoServiceProvider()
        Dim count = inputList.Count
        While count > 1
            Dim bytes = New Byte(0) {}
            Do
                cryptoServiceProvider.GetBytes(bytes)
            Loop While Not (bytes(0) < count * ([Byte].MaxValue / count))
            Dim index = (bytes(0) Mod count)
            count -= 1
            Dim input = inputList(index)
            inputList(index) = inputList(count)
            inputList(count) = input
        End While
        Return inputList
    End Function

    Private Sub AssignMySettings()
        My.Settings.FacebookUser = FacebookUserText.Text
        My.Settings.FacebookPass = FacebookPassText.Text
        My.Settings.WeblikeUser = WeblikeUserText.Text
        My.Settings.WeblikePassword = WeblikePassText.Text
        My.Settings.FacebookCheck = RememberMe.Checked
    End Sub

    Private Sub webComboInit()
        'Dim comboSource As New Dictionary(Of String, String)()
        'For Each list As String In UserObj.getWeblists : comboSource.Add(list, list) : Next
        'WebComboBox.DataSource = New BindingSource(comboSource, Nothing)
        'WebComboBox.DisplayMember = "Value"
        'WebComboBox.ValueMember = "Key"
    End Sub

    Private Sub userInfoInit()
        If My.Settings.FacebookCheck Then
            FacebookUserText.Text = My.Settings.FacebookUser
            FacebookPassText.Text = My.Settings.FacebookPass
            WeblikeUserText.Text = My.Settings.WeblikeUser
            WeblikePassText.Text = My.Settings.WeblikePassword
        End If
        RememberMe.Checked = My.Settings.FacebookCheck
    End Sub

    Private Sub WebComboBox_SelectedIndexChanged(sender As Object, e As EventArgs)
    End Sub

    Private Sub CheckboxIsChecked(sender As Object, e As EventArgs)
        If Not FBChkList.Contains(sender.name.ToString) Then Exit Sub
        Dim chk = CType(Me.Controls.Item(sender.name.ToString), CheckBox)
        Dim length As Integer = sender.name.ToString.Count
        Dim rmode = CType(Me.Controls.Item(sender.name.ToString.Substring(0, length - 8) & "Round"), NumericUpDown)
        If chk.Checked Then rmode.Enabled = True Else rmode.Enabled = False
    End Sub

    Private Sub addFBMode()
        Dim r As Integer = 0
        For Each mode As String In WeblikeObj.getMode()
            addCheckbox(mode, r)
            addRoundbox(mode, r)
            r += 1
        Next
    End Sub

    Private Sub addRoundbox(ByVal name As String, ByVal r As Integer)
        Dim num As NumericUpDown
        num = New NumericUpDown
        num.Enabled = False
        num.Location = New System.Drawing.Point(330, (r * 25) + 120)
        num.Name = name & "Round"
        num.Size = New System.Drawing.Size(39, 20)
        Me.Controls.Add(num)
        FBRoundList.Add(num.Name.ToString)
    End Sub

    Private Sub addCheckbox(ByVal name As String, ByVal r As Integer)
        Dim chk As CheckBox
        chk = New CheckBox
        chk.AutoSize = True
        chk.BackColor = System.Drawing.SystemColors.HighlightText
        chk.ForeColor = System.Drawing.Color.Black
        chk.Location = New System.Drawing.Point(208, (r * 25) + 120)
        chk.Name = name & "Checkbox"
        chk.Size = New System.Drawing.Size(104, 17)
        chk.Text = name
        chk.UseVisualStyleBackColor = False
        AddHandler chk.CheckedChanged, AddressOf CheckboxIsChecked
        Me.Controls.Add(chk)
        FBChkList.Add(chk.Name.ToString)
    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Dim r As Run
        r = New Run(Fbmode)
        Application.Run(r)
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        AutobotForm.Show()
    End Sub

End Class