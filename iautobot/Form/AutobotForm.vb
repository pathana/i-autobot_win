﻿Imports System.ComponentModel
Imports System.IO
Imports Newtonsoft.Json.Linq
Public Class AutobotForm
    Private Sub AutobotForm_Closing(sender As Object, e As FormClosingEventArgs) Handles Me.Closing
        Dim dictData As New Dictionary(Of String, Object)
        dictData.Add("key", My.Settings.Key)
        Dim jsonData = Api.postData(dictData, My.Settings.URL & "api/logout.php")
        If Not jsonData.ContainsKey("data") Then
            MsgBox("ไม่สามาร logout ได้ ไม่สามารถติดต่อ server ได้")
            e.Cancel = True
            Exit Sub
        End If
        Dim status As JObject = jsonData.Item("data")
        If CInt(status.Item("status")) = 0 Then LoginForm1.Show() : Exit Sub
        MsgBox("ไม่สามารถ logout ได้")
        e.Cancel = True
    End Sub

    Private Sub AutobotForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        FacebookForm.Show()
        FacebookForm.MdiParent = Me
        Text = Text & version
    End Sub

End Class