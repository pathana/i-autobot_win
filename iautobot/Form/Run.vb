﻿Imports OpenQA.Selenium
Imports OpenQA.Selenium.PhantomJS
Imports OpenQA.Selenium.Support
Imports System.Threading
Imports System.ComponentModel
Imports Newtonsoft.Json.Linq

Public Class Run
    Private FBmode As List(Of String) = Nothing
    Dim autobot_run As AutoBot
    Public windowcrash As Boolean
    Public CancelMsg As String = "ยกเลิกกลางคัน"

    Public Sub New(ByVal mode As List(Of String))
        InitializeComponent()
        FBmode = mode
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub

    Delegate Sub SetTextCallback(ByVal text As String)
    Delegate Sub SetLogsCallback(ByVal text As String, ByVal color As Color)
    Delegate Sub ClearRichTextCallback()

    Private Sub Run_Load(sender As Object, e As EventArgs) Handles Me.Load
        Text = Text & version
        LoopAction.WorkerSupportsCancellation = True
        LoopAction.WorkerReportsProgress = True
        LoopAction.RunWorkerAsync()
    End Sub

    Public Sub SetText(ByVal text As String)
        If Me.Label1.InvokeRequired Then
            Dim d As New SetTextCallback(AddressOf SetText)
            Me.Invoke(d, New Object() {text})
        Else
            Me.Label1.Text = "Weblike :" & text
        End If
    End Sub

    Public Sub SetCoins(ByVal text As String)
        If Me.Label3.InvokeRequired Then
            Dim d As New SetTextCallback(AddressOf SetCoins)
            Me.Invoke(d, New Object() {text})
        Else
            Me.Label3.Text = "เหรียญ :" & text
        End If
    End Sub

    Public Sub SetMode(ByVal text As String)
        If Me.Label2.InvokeRequired Then
            Dim d As New SetTextCallback(AddressOf SetMode)
            Me.Invoke(d, New Object() {text})
        Else
            Me.Label2.Text = "Mode :" & text
        End If
    End Sub

    Public Sub SetClick(ByVal text As String)
        If Me.Label4.InvokeRequired Then
            Dim d As New SetTextCallback(AddressOf SetClick)
            Me.Invoke(d, New Object() {text})
        Else
            Me.Label4.Text = "คลิ๊กวันนี้ :" & text
        End If
    End Sub

    Public Overloads Sub SetLogs(ByVal text As String, ByVal color As Color)
        Try
            If text.Length + Me.RichTextBox1.TextLength < Me.RichTextBox1.MaxLength Then
                Me.RichTextBox1.SelectionColor = color
                Me.RichTextBox1.AppendText(text & Environment.NewLine)
                Me.RichTextBox1.ScrollToCaret()
            Else
                Me.RichTextBox1.Text = String.Empty
                Me.RichTextBox1.SelectionColor = color
                Me.RichTextBox1.AppendText(text & Environment.NewLine)
                Me.RichTextBox1.ScrollToCaret()
            End If
        Catch ex As Exception
            CancelMsg = "Memory เต็มไม่สามารถใช้งานต่อได้"
            LoopAction.CancelAsync()
        End Try
    End Sub

    Public Overloads Sub SetLogs(ByVal text As String)
        Try
            If text.Length + Me.RichTextBox1.TextLength < Me.RichTextBox1.MaxLength Then
                Me.RichTextBox1.SelectionColor = Color.White
                Me.RichTextBox1.AppendText(text & Environment.NewLine)
                Me.RichTextBox1.ScrollToCaret()
            Else
                Me.RichTextBox1.Text = String.Empty
                Me.RichTextBox1.SelectionColor = Color.White
                Me.RichTextBox1.AppendText(text & Environment.NewLine)
                Me.RichTextBox1.ScrollToCaret()
            End If
        Catch ex As Exception
            CancelMsg = "Memory เต็มไม่สามารถใช้งานต่อได้"
            LoopAction.CancelAsync()
        End Try
    End Sub

    Public Sub ClearRichText()
        If Me.RichTextBox1.InvokeRequired Then
            Dim d As New ClearRichTextCallback(AddressOf ClearRichText)
            Me.Invoke(d)
        Else
            Me.RichTextBox1.Text = String.Empty
        End If
    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles LoopAction.DoWork
        autobot_run = New AutoBot(Me, CType(sender, System.ComponentModel.BackgroundWorker), e)
        If LoopAction.CancellationPending = True Then e.Cancel = True : Exit Sub
        autobot_run.Main()
        autobot_run.Init()
        Me.SetText(WeblikeObj.getUrl.ToString)
        windowcrash = False
        For Each mode As String In FBmode
            If Me.windowcrash Then RecoverDriver()
            If LoopAction.CancellationPending = True Then e.Cancel = True : Exit Sub
            If Not KeyAvailable() Then e.Cancel = True : Exit Sub
            Me.SetMode(mode)
            ClearRichText()
            Dim klass = Activator.CreateInstance(Type.GetType("iAutobot." & mode), Me, CType(sender, System.ComponentModel.BackgroundWorker), e)
            Try
                klass.Start()
            Catch ex As Exception
            End Try
        Next
    End Sub

    Private Sub RecoverDriver()
        autobot_run.Main()
        autobot_run.Init()
        Me.windowcrash = False
    End Sub

    Private Sub BackgroundWorker1_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles LoopAction.ProgressChanged
        Dim hash As Hashtable = CType(e.UserState, Hashtable)
        Dim message As String = hash.Item("message")
        Dim color As Color = hash.Item("color")
        Me.SetLogs(message, color)
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles LoopAction.RunWorkerCompleted
        driver.Quit()
        PictureBox1.Visible = False
        PictureBox2.Visible = True
        Button1.Visible = False
        Beep()
        MsgBox("การทำงานเสร็จสิ้น")
        If e.Error IsNot Nothing Then
            Error_logs(e.Error.Message)
            SetLogs("Error: " & e.Error.Message, Color.Red)
            Exit Sub
        End If
        If e.Cancelled Then SetLogs(CancelMsg, Color.Orange) : Exit Sub
        SetLogs("โปรแกรมทำงานเสร็จสิ้น", Color.Lime)
    End Sub

    Private Sub Cancle_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.SetLogs("รอซักครู่ กำลังหยุดการทำงานของโปรแกรม", Color.Orange)
        If Not Me.LoopAction.IsBusy Then Me.Dispose() : Exit Sub
        Try
            CancelMsg = "ยกเลิกโดยผู้ใช้งาน"
            Me.LoopAction.CancelAsync()
        Catch ex As InvalidOperationException
            Me.SetLogs("ไม่สามารถปิดโปรแกรมได้", Color.Red)
        End Try
    End Sub

    Public Shared Function KeyAvailable() As Boolean
        Dim dictData As New Dictionary(Of String, Object)
        dictData.Add("key", My.Settings.Key)
        Dim jsonData = Api.postData(dictData, My.Settings.URL & "api/available.php")
        If jsonData.ContainsKey("data") Then
            Dim data As JObject = jsonData.Item("data")
            If CType(data.Item("available"), Integer) = 1 Then Return True Else Return False
        End If
        Return False
    End Function

    Public Shared Sub Error_logs(ByVal error_msg As String)
        Dim hashData As New Hashtable
        hashData.Add("error", error_msg)
        hashData.Add("key", My.Settings.Key)
        Api.postData(hashData, My.Settings.URL & "api/error_logs.php")
    End Sub

    Private Sub Run_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        Me.SetLogs("รอซักครู่ กำลังหยุดการทำงานของโปรแกรม", Color.Orange)
        If Not Me.LoopAction.IsBusy Then Me.Dispose() : Exit Sub
        Try
            CancelMsg = "ยกเลิกโดยผู้ใช้งาน"
            Me.LoopAction.CancelAsync()
        Catch ex As InvalidOperationException
            Me.SetLogs("ไม่สามารถปิดโปรแกรมได้", Color.Red)
        End Try
    End Sub
End Class