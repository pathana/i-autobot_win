﻿Imports OpenQA.Selenium.Chrome
Imports OpenQA.Selenium.Firefox
Imports OpenQA.Selenium.PhantomJS

Public Class WebDrivers
    Public Shared Sub CreateFirefox()
        driver = New FirefoxDriver()
    End Sub

    Public Shared Sub CreatePhantomJS()
        Dim options As PhantomJSOptions = New PhantomJSOptions
        Dim userAgent As String = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0"
        Dim driverService = PhantomJSDriverService.CreateDefaultService
        options.AddAdditionalCapability("phantomjs.page.settings.userAgent", userAgent)
        driverService.HideCommandPromptWindow = True
        driverService.LoadImages = False
        driver = New PhantomJSDriver(driverService, options, TimeSpan.FromMinutes(2))
    End Sub

    Public Shared Sub CreateChrome()
        Dim service = ChromeDriverService.CreateDefaultService
        service.HideCommandPromptWindow = False
        Dim options As ChromeOptions = New ChromeOptions
        options.AddArgument("--disable-notifications")
        driver = New ChromeDriver(service, options)
    End Sub
End Class
