﻿Imports OpenQA.Selenium
Imports OpenQA.Selenium.Support.UI

Public Class FindElement
    Public Shared Function ElementHas(ByVal find As By) As IWebElement
        Dim element
        Try
            element = driver.FindElement(find)
        Catch ex As Exception
            element = Nothing
        End Try
        Return element
    End Function

    Public Shared Function ElementHave(ByVal find As By) As IList(Of IWebElement)
        Dim elements
        Try
            elements = driver.FindElements(find)
        Catch ex As Exception
            elements = Nothing
        End Try
        Return elements
    End Function

    Public Shared Function WaitElement(ByVal find As By) As IWebElement
        Dim element
        Try
            Dim wait As WebDriverWait = New WebDriverWait(driver, TimeSpan.FromSeconds(5))
            element = wait.Until(ExpectedConditions.ElementIsVisible(find))
        Catch ex As Exception
            element = Nothing
        End Try
        Return element
    End Function

    Public Shared Function WaitElement(ByVal find As By, time As Integer) As IWebElement
        Dim element
        Try
            Dim wait As WebDriverWait = New WebDriverWait(driver, TimeSpan.FromSeconds(time))
            element = wait.Until(ExpectedConditions.ElementIsVisible(find))
        Catch ex As Exception
            element = Nothing
        End Try
        Return element
    End Function

    Public Shared Function WaitToClick(ByVal find As IWebElement) As IWebElement
        Dim element
        Try
            Dim wait As WebDriverWait = New WebDriverWait(driver, TimeSpan.FromSeconds(5))
            element = wait.Until(ExpectedConditions.ElementToBeClickable(find))
        Catch ex As Exception
            element = Nothing
        End Try
        Return element
    End Function
End Class


