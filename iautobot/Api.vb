﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Module Api
    Public Function postData(ByVal dictData As Dictionary(Of String, Object), ByVal uri As String) As Dictionary(Of String, Object)
        Dim webClient As New WebClient()
        Dim resByte As Byte()
        Dim resString As String
        Dim reqString() As Byte

        Try
            webClient.Headers("content-type") = "application/json"
            reqString = Encoding.Default.GetBytes(JsonConvert.SerializeObject(dictData, Formatting.Indented))
            resByte = webClient.UploadData(uri, "post", reqString)
            resString = Encoding.Default.GetString(resByte)
            dictData = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(resString)
            webClient.Dispose()
        Catch ex As Exception
            dictData.Clear()
        End Try
        Return dictData
    End Function

    Public Sub postData(ByVal hashData As Hashtable, ByVal uri As String)
        Dim webClient As New WebClient()
        Dim resByte As Byte()
        Dim reqString() As Byte
        Try
            webClient.Headers("content-type") = "application/json"
            reqString = Encoding.Default.GetBytes(JsonConvert.SerializeObject(hashData, Formatting.Indented))
            resByte = webClient.UploadData(uri, "post", reqString)
        Catch ex As Exception
        End Try
    End Sub
End Module
